import json

import pandas as pd

##############
# Deprecated #
##############


def inspect_multiple_matches(matches):
    wos_multi_field = matches.groupby(
        ["wos_standard", "wos_first", "wos_daisng_id"]
    ).agg(list)[lambda df: df["field_first"].map(len).gt(1)]
    field_multi_wos = matches.groupby(["field_first", "field_last"]).agg(list)[
        lambda df: df["wos_standard"].map(len).gt(1)
    ]
    return wos_multi_field, field_multi_wos


def add_match_to_authors(authors, field_names):
    authors["match"] = authors.agg(
        lambda x: max(
            match_field_and_wos_names(first, last, x["wos_standard"], x["author_first"])
            for _, (first, last) in field_names[["first", "last"]].iterrows()
        ),
        axis=1,
    )


def filter_authors_match_field_name(authors, first, last):
    match = authors.map(
        lambda x: match_field_and_wos_names(
            first, last, x["wos_standard"], x["author_first"]
        ),
        axis=1,
    )
    return authors[match]


def matched_authors(authors):
    return authors[authors["match"].astype(bool)]


def filter_authors_matched_ok(authors, ok=True):
    return authors[
        authors.agg(
            lambda x: ((x["author_first"] is not None) is ok) and x["match"], axis=1
        ).astype(bool)
    ]


# flid[lambda x: x.map(lambda y: len([yy for yy in y if yy is not None]) > 1)]


def field_first_last_to_wos_daisng_ids(matches):
    flid = matches.groupby(["field_first", "field_last"])["wos_daisng_id"].agg(
        lambda x: {None if pd.isna(xx) else int(xx) for xx in x}
    )
    return flid


def add_wos_daisng_id_to_field_names(field_names, matches):
    flid = field_first_last_to_wos_daisng_ids(matches)
    field_names["wos_daisng_id"] = field_names[["first", "last"]].agg(
        lambda x: flid.loc[tuple(x)] if tuple(x) in flid.index else None, axis=1
    )


def get_doc_uids(matches, corpus_data):
    def sel(matches_group_df, corpus_data_row):
        authors_daisng_id = corpus_data_row["authors_daisng_id"]
        if res := matches_group_df["wos_daisng_id"].isin(authors_daisng_id).any():
            return res
        return False

    return matches.groupby(["field_first", "field_last"]).apply(
        lambda df: corpus_data.loc[
            corpus_data.agg(lambda row: sel(df, row), axis=1), "uid"
        ].to_list()
    )


def field_first_last_wos_daisng_id_to_doc_uids(flid, corpus_data):
    """
    flid ← field_first_last_to_wos_daisng_ids()
    """
    flid_uids = flid.reset_index().map(
        lambda row: corpus_data.loc[
            lambda df: df["authors_daisng_id"].map(
                lambda x: any(xx in row["wos_daisng_id"] for xx in x)
            )
            | (None if None in row["wos_daisng_id"] else 0),
            "uid",
        ],
        axis=1,
    )
    return flid_uids


def check(authors_docs_positions_with_field_names, names_uids):
    afluid = authors_docs_positions_with_field_names
    uid_diff = afluid.to_frame().agg(
        lambda row: row["uids"].difference(names_uids.loc[row.name]), axis=1
    )
    return uid_diff


##############
# Deprecated #
##############


class OldFieldNames:
    def field_names_as_df(remove_diacritics=True):
        with open("data/names.json") as f:
            data = json.load(f)
        df = pd.DataFrame(data=data, columns=["last", "first", "source"])
        df = df.transform(lambda x: x.str.casefold())
        df = df.transform(lambda x: x.str.replace(",", " ", regex=False))
        df = df.transform(lambda x: x.str.replace(".", " ", regex=False))
        df = df.transform(lambda x: x.str.strip())
        df = df.transform(lambda x: x.str.replace(r"\s+", " ", regex=True))
        if remove_diacritics:
            df = df.transform(lambda s: s.str.translate(str.maketrans(charmap())))
        df = df.drop_duplicates()
        df = df.groupby(["last", "first"]).agg(list).reset_index()
        return df

    @classmethod
    def merge_diacritic_names(cls):
        names = cls.field_names_as_df(remove_diacritics=False)
        grouped = names.groupby(
            [
                names["last"].str.translate(str.maketrans(charmap())),
                names["first"].str.translate(str.maketrans(charmap())),
            ]
        )
        merged = grouped.agg(
            {
                "last": lambda x: sorted(x)[-1],
                "first": lambda x: sorted(x)[-1],
                "source": lambda x: list(set(item for xx in x for item in xx)),
            }
        ).reset_index(drop=True)
        return merged


def get_main_authors():
    authors = [
        "aiassa, d",
        "manas, f",
        "simoniello, mf",
        "oliva, a",
        "montenegro, ra",
        "carrasco, ae",
        "marino, djg",
        "marino, d",
        "marino, dj",
        "lajmanovich, r",
        "lajmanovich, rc",
        "pengue, wa",
        "sarandon, sj",
    ]
    return authors
