import graph_tool

from . import data, analyses
from .data.names import get_field_names_documents

del graph_tool

__all__ = [
    "data",
    "analyses",
    "get_field_names_documents",
]

__version__ = "0.1"
