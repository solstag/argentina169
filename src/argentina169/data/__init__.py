import logging
import re

import pandas as pd

from bibliodbs.query_downloader import (
    WOSQueryDownloader,
    CallTimeHandler,
    get_query_sizes,
)

from .biblio_wos_hack import load_extracted_cleaned_and_enriched

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

try:
    from spacy.lang.en import English
    from spacy.util import compile_infix_regex
except ImportError:
    logger.warning("Spacy could not be imported: tokenization will be unavailable.")


WOS_API_KEY = None
logger.warning(
    f"Please set api key with `{__package__}.WOS_API_KEY = {{your api key}}`"
)

QUERY_AND = " \nAND "

TOPIC_NAMES = [
    # generic
    "pesticide",
    "herbicide",
    "insecticide",
    "fungicide",
    # glypho
    "glyphosate",
    "glifosato",
    "roundup",
    "roundup ready",  # redundant with "roundup"
    "aminomethylphosphonic acid",
    "alpha-amino-3-hydroxy-5-methyl-4-isoxazolepropionic acid",
    # uni
    "DDT",
    # duo
    "endosulfan",
    "endosulfán",
    # duo
    "pirimiphos-methyl",
    "pirimifós metil",
    # uni
    "chlorpyrifos",
    "pirimicarb",
    "carbofuran",
    # duo
    "cypermethrin",
    "cipermetrina",
    # uni
    "deltamethrin",
    "imidacloprid",
    "dicamba",
    # duo
    "paraquat",
    "methyl viologen",
    # duo
    "2,4-D",
    "2,4-dichlorophenoxyacetic acid",
    # duo
    "atrazine",
    "atrazina",
    # duo
    "glufosinate",
    "glufosinato",
]


def gen_query(topic_names=None):
    if topic_names is None:
        topic_names = TOPIC_NAMES
    queries, query_parts = {}, {}
    query_parts["country"] = """ CU=(Argentina) """
    query_parts["doctype"] = """ DT=(Article) """
    query_parts["address"] = """ AD=(Argentina) """  # matches streets named Argentina
    query_parts["topics"] = " TS=(" + " OR ".join(topic_names) + ") "

    queries["WOS"] = (
        QUERY_AND.join([query_parts[x] for x in ("country", "doctype", "topics")]) + " "
    )
    queries["MEDLINE"] = (
        QUERY_AND.join([query_parts[x] for x in ("address", "topics")]) + " "
    )
    return queries, query_parts


def get_wqd(query, outpath, **kwargs):
    return WOSQueryDownloader(WOS_API_KEY, query, outpath=outpath, **kwargs)


def get_wos_wqd():
    queries, _ = gen_query()
    outpath = "data/wos_api-WOS.jsonl"
    database_id = "WOS"
    wqd = get_wqd(queries[database_id], database_id=database_id, outpath=outpath)
    return wqd


def get_wos_topics_counts():
    _, query_parts = gen_query()
    queries = {
        name: QUERY_AND.join(
            [query_parts["country"], query_parts["doctype"], f"TS=({name})"]
        )
        for name in TOPIC_NAMES
    }
    return get_query_sizes(WOSQueryDownloader, WOS_API_KEY, queries, database_id="WOS")


def compare_databases():
    call_time = CallTimeHandler(0)
    queries, _ = gen_query()
    wos = get_wqd(
        queries["WOS"],
        api_last_call_time=call_time,
        database_id="WOS",
        viewField="WOS+identifier",
        optionView="FS",
    )
    medline = get_wqd(
        queries["MEDLINE"],
        api_last_call_time=call_time,
        database_id="MEDLINE",
        viewField="MEDLINE+identifier",
        optionView="FS",
    )
    return wos, medline


def load_data_as_frame():
    wqd = get_wos_wqd()
    if wqd.store_records is not None:
        wqd.download()
    data = [*load_extracted_cleaned_and_enriched(wqd._outpath)]
    df = pd.DataFrame(data)
    df.name = "argentina169"
    return df


def get_tokenizer():
    nlp = English()
    nlp.add_pipe("sentencizer")
    infixes = nlp.Defaults.infixes.copy()
    # do not split on simple hyphen
    spot = infixes[-2].index("(?:-|–|—|--|---|——|~)")
    infixes[-2] = infixes[-2][: spot + 3] + infixes[-2][spot + 5 :]
    # do split on relation followed by numeral
    spot = infixes[-1].rindex("[:<>=/](?=[")
    infixes[-1] = infixes[-1][: spot + 11] + "0-9" + infixes[-1][spot + 11 :]
    # compile and replace
    infix_rx = compile_infix_regex(infixes)
    nlp.tokenizer.infix_finditer = infix_rx.finditer
    return nlp


def text_to_tokens(df, source_columns, target_column):
    nlp = get_tokenizer()
    rx_alpha = re.compile(r"[^\W\d_]")

    def word_filter(tok):
        return (
            len(tok) > 1
            and tok.text.casefold() not in nlp.Defaults.stop_words
            and rx_alpha.search(tok.text)
        )

    df[target_column] = (
        df[source_columns]
        .map(
            lambda txt: [
                [tok.text for tok in sent if word_filter(tok)]
                for sent in nlp(txt).sents
            ],
            na_action="ignore",
        )
        .agg(lambda row: [sen for doc in row.dropna() for sen in doc], axis=1)
        .map(lambda doc: [[wor.casefold() for wor in sen] for sen in doc])
    )


def ngram_tokens(df, column, ngrams=3):
    from gensim.models import phrases

    phrases_args = {"scoring": "npmi", "threshold": 0.9}

    tokens = df[column]
    if ngrams > 1:
        bigram = phrases.Phraser(
            phrases.Phrases((s for d in tokens for s in d), **phrases_args)
        )
        get_ngram = lambda sen: [*bigram[sen]]  # noqa
    if ngrams > 2:
        trigram = phrases.Phraser(
            phrases.Phrases(bigram[(s for d in tokens for s in d)], **phrases_args)
        )
        get_ngram = lambda sen: [*trigram[bigram[sen]]]  # noqa
    df[column] = df[column].map(get_ngram)


def authors_casefold(df):
    df["authors_wos_standard"] = (
        df["authors_wos_standard"].explode().str.casefold().groupby(level=0).agg(list)
    )
    df["authors_first_name"] = (
        df["authors_first_name"].explode().str.casefold().groupby(level=0).agg(list)
    )


def enhance_title(df):
    df["enhanced_title"] = df.agg(
        lambda x: x["title"] + " [" + "; ".join(x["authors_wos_standard"]) + "]", axis=1
    )


def load_coded_database():
    df = pd.read_excel(
        "../coded_database/Florencia's database (with WOS ID).xlsx", header=17
    )
    return df


def match_current_vocabulary(corpus):
    """
    Used when corpus text has been modified/cleaned a posteriori.
    (As load_domain_topic_model() only triggers trim_to_sample() if sampled on docs)
    """
    corpus.tblocks = corpus.tblocks[corpus.tblocks.index.isin(corpus.ter_documents)]
