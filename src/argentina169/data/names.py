"""
Deal with the matching of names between
- field data: first, last
- wos data: wos_standard, [daisng_id], [first], last
In order to identify articles by field actors:
1. articles for which wos first exists and names match
2. articles for which daisngid of matched wos names match
"""

import logging
import pandas as pd
from tqdm import tqdm
from unidecode import unidecode

logger = logging.getLogger(__name__)
logger.setLevel(logging.WARNING)


def get_field_names_documents(corpus):
    field_names = get_field_names()
    matches = get_names_authors_matches(field_names, corpus.data)
    field_names_docs = gen_field_names_documents(matches, corpus.data)

    # Add the column to both current and original data
    add_field_names_to_doc_data(corpus.full_data, field_names_docs)
    return field_names_docs


def add_field_names_to_doc_data(
    corpus_data, field_names_docs, field_names_col="field_names"
):
    gby = field_names_docs.groupby("doc_uid")[["field_first", "field_last"]]
    names_to_docs = gby.apply(
        lambda df: df.agg(
            lambda row: f"{row['field_last']}, {row['field_first']}", axis=1
        ).to_list()
    )
    corpus_data[field_names_col] = names_to_docs.reindex(corpus_data.uid).values


def get_field_names(remove_diacritics=True):
    df = pd.read_json("data/names_merged.json", orient="table")
    name_cols = ["first", "last"]
    if remove_diacritics:
        df[name_cols] = df[name_cols].transform(lambda s: s.map(unidecode))
    if df[["last", "first"]].duplicated().any():
        raise ValueError()
    df["first_year"] = (
        df["source"]
        .map(
            lambda x: pd.Series(x)
            .str.extract(r"(\d\d\d\d)$")[0]
            .map(int, na_action="ignore")
            .min()
        )
        .astype("Int64")
    )
    return df


def get_names_authors_matches(field_names, corpus_data, valid=True):
    wos_authors = get_authors(corpus_data)
    matches = []
    for _, (field_first, field_last) in tqdm(
        field_names[["first", "last"]].iterrows(), total=len(field_names)
    ):
        for _, wos_author in wos_authors.iterrows():
            args = (
                field_first,
                field_last,
                wos_author["authors_wos_standard"],
                wos_author["authors_first_name"],
            )
            if match_field_and_wos_names(*args):
                matches.append(args + (wos_author["authors_daisng_id"],))
    matches = pd.DataFrame(
        matches,
        columns=[
            "field_first",
            "field_last",
            "wos_standard",
            "wos_first",
            "wos_daisng_id",
        ],
        dtype="object",
    )
    return get_valid_matches(matches) if valid else matches


def get_authors(corpus_data):
    authors = (
        pd.concat(
            [corpus_data[x].explode() for x in corpus_data if x.startswith("author")],
            axis=1,
        )
        .drop_duplicates()
        .reset_index(drop=True)
    )
    return authors


def match_field_and_wos_names(
    field_first, field_last, wos_standard, wos_first_of_first
):
    # Deal with WOS issues
    wos_standard = wos_standard.replace("' ", "'")
    if ", " not in wos_standard and wos_standard[-2] == " ":
        wos_standard = wos_standard[:-2] + "," + wos_standard[-2:]
    if ", " not in wos_standard:
        return False
    wos_last, wos_first_as_initials = wos_standard.split(", ")

    # Process field first and last
    field_first_of_first = field_first.split()[0]
    field_first_as_initials = [x[0] for x in field_first.split(" ")]
    field_last_as_list = field_last.split(" ")

    # Match wos and field names
    if (
        # First of first names must match, if available
        (wos_first_of_first is None or wos_first_of_first == field_first_of_first)
        # Initials must match, up to shortest
        and all(x == y for x, y in zip(field_first_as_initials, wos_first_as_initials))
        # Last names must match, or field last of last, which can be preceded by particle
        and (
            wos_last == field_last
            or wos_last == field_last_as_list[-1]
            or (
                len(field_last_as_list) > 1
                and field_last_as_list[-2] == "de"
                and wos_last == " ".join(field_last_as_list[-2:])
            )
        )
    ):
        logger.info(
            f"Matched: ({field_first}, {field_last}) ~ ({wos_standard}, {wos_first_of_first})"
        )
        # Return the number of matching initials
        return min(len(field_first_as_initials), len(wos_first_as_initials))
    # Did not match
    return False


def get_valid_matches(matches):
    valid_matches = matches.dropna(subset=["wos_first"])
    return valid_matches


def gen_field_names_documents(matches: pd.DataFrame, corpus_data: pd.DataFrame):
    def explode(row: pd.Series) -> pd.DataFrame:
        return row.to_frame().T.explode([*row.index])

    def get_documents(match_author_df) -> pd.Index:
        # Here .dropna() will remove rows missing any of first and standard forms
        match_first_standard = match_author_df[["wos_first", "wos_standard"]].dropna()
        match_daisng_id = match_author_df["wos_daisng_id"].dropna()

        def doc_selector(doc_authors_row: pd.Series) -> bool:
            doc_first_standard = explode(
                doc_authors_row[["authors_first_name", "authors_wos_standard"]]
            )
            if (
                doc_authors_row[["authors_daisng_id"]]
                .explode()
                .isin(match_daisng_id)
                .any()
            ):
                logger.debug("Doc matched by ID")
                return True
            if (
                doc_first_standard.agg(list, axis=1)
                .isin(match_first_standard.agg(list, axis=1))
                .any()
            ):
                logger.debug("Doc matched by author names")
                return True
            return False

        return corpus_data[
            ["authors_first_name", "authors_wos_standard", "authors_daisng_id"]
        ][lambda df: df.agg(doc_selector, axis=1)].index

    ret = matches.groupby(["field_first", "field_last"]).apply(get_documents)
    ret = ret.rename("doc_index").explode().reset_index()
    ret["doc_uid"] = ret["doc_index"].map(corpus_data["uid"].get)
    return ret
