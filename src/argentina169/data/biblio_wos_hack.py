import logging
from bibliodbs import biblio_wos
from bibliodbs.biblio_wos import listit

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def load_extracted_cleaned_and_enriched(*args, **kwargs):
    OrigRecordExtractor = biblio_wos.RecordExtractor
    try:
        biblio_wos.RecordExtractor = LocalRecordExtractor
        yield from biblio_wos.load_extracted_cleaned_and_enriched(*args, **kwargs)
    finally:
        biblio_wos.RecordExtractor = OrigRecordExtractor


class LocalRecordExtractor(biblio_wos.RecordExtractor):
    def get_authors(self):
        authors_wos_standard = []
        authors_daisng_id = []
        authors_full_name = []
        authors_first = []
        for obj in listit(self.summary_data["names"]["name"]):
            # HACK TO CARRY ON LEGACY DATA ASSUMING IT'S HARMLESS
            # if obj["role"] == "author":
            if True:
                authors_wos_standard.append(obj.get("wos_standard", None))
                authors_daisng_id.append(obj.get("daisng_id", None))
                authors_full_name.append(obj.get("full_name", None))
                authors_first.append(get_first_name(obj))
        return {
            "authors_wos_standard": authors_wos_standard,
            "authors_daisng_id": authors_daisng_id,
            "authors_full_name": authors_full_name,
            "authors_first_name": authors_first,
        }


def get_first_name(name_data):
    for target, get_name in {
        "pref_first": lambda x: x["preferred_name"]["first_name"],
        "first": lambda x: x["first_name"],
        "display": lambda x: x["display_name"],
        "full": lambda x: x["full_name"],
    }.items():
        try:
            name = get_name(name_data)
        except Exception:
            continue
        message = f'Found {target}: "{name}". '
        name = name.split(", ")[-1]
        name = name.split()[0]
        message += f'Turned: "{name}". '
        if "." not in name and len(name) > 1 and not name.isupper():
            logger.info(message + "Returned.")
            return name
        else:
            logger.info(message + "Dropped.")
