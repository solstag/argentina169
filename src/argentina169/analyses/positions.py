import pandas as pd

#############
# PRECOCITY #
#############


def get_field_names_docs_precocity(
    fnd_positions: pd.DataFrame, corpus: pd.DataFrame, store=True
):
    fnd_precocity = fnd_positions.loc[
        :,
        (
            fnd_positions.columns.levels[0],
            ["", "N", "fraction"],
            ["", "before"],
        ),
    ]
    cols_to_add = ["title", "year", "url_wos"]
    fnd_precocity[cols_to_add] = (
        corpus.data.set_index("uid")
        .loc[fnd_precocity["doc_uid"], cols_to_add]
        .to_numpy()
    )
    fnd_precocity = fnd_precocity[
        fnd_precocity.columns[-5:-3]
        .append(fnd_precocity.columns[-6:-5])
        .append(fnd_precocity.columns[-3:])
        .append(fnd_precocity.columns[:-6])
    ]
    for level in corpus.dblocks_levels:
        fnd_precocity[("Domain", f"L{level}D", "")] = (
            corpus.dblocks.set_index(corpus.data["uid"])
            .loc[fnd_precocity["doc_uid"], level]
            .map(lambda block: corpus.lblock_to_label[level, block])
            .to_numpy()
        )
    if store:
        fnd_precocity.to_excel("field_names_docs_precocity.xlsx")

    return fnd_precocity


#############
# POSITIONS #
#############


def get_empty_doc_positions(levels):
    return pd.DataFrame(
        columns=pd.MultiIndex.from_tuples(
            [
                (level, kind, place)
                for level in levels
                for kind in ["N", "count", "fraction"]
                for place in (["before", "at", "after"] if kind != "N" else [""])
            ]
        )
    )


def get_doc_positions(doc_idx, corpus):
    doc_positions = get_empty_doc_positions(corpus.dblocks_levels)
    doc_date = corpus.data.loc[doc_idx, corpus.col_time]
    for level in corpus.dblocks_levels:
        db = corpus.dblocks.loc[doc_idx, level]
        db_docs_time = corpus.data.loc[corpus.dblocks[level].eq(db), corpus.col_time]
        date_counts = db_docs_time.value_counts()
        num = len(db_docs_time)
        count = [
            date_counts.loc[date_counts.index < doc_date].sum(),
            date_counts.loc[date_counts.index == doc_date].sum(),
            date_counts.loc[date_counts.index > doc_date].sum(),
        ]
        fraction = [x / len(db_docs_time) for x in count]
        doc_positions.at[doc_idx, (level, "N")] = num
        doc_positions.at[doc_idx, (level, "count")] = count
        doc_positions.at[doc_idx, (level, "fraction")] = fraction
    return doc_positions


def get_docs_positions(doc_idxs, corpus):
    docs_positions = (
        pd.concat([get_doc_positions(doc_idx, corpus) for doc_idx in doc_idxs])
        if len(doc_idxs)
        else get_empty_doc_positions(corpus.dblocks_levels)
    )
    return docs_positions


def field_names_documents_with_positions(field_names_docs: pd.DataFrame, corpus):
    doc_idxs = field_names_docs["doc_index"].unique()

    docs_positions = get_docs_positions(doc_idxs, corpus)
    docs_positions = docs_positions.loc[field_names_docs["doc_index"]]

    field_names_docs = field_names_docs.drop("doc_index", axis=1)
    field_names_docs.columns = pd.MultiIndex.from_tuples(
        (x, "", "") for x in field_names_docs.columns
    )

    ret = field_names_docs.join(docs_positions.set_index(field_names_docs.index))
    return ret
