from __future__ import annotations
import typing

from sashimi.blocks.better_network_map import network_map

from . import sashimi, positions

__all__ = ["sashimi", "positions"]

if typing.TYPE_CHECKING:
    import sashimi as shmi


def get_nonactivist_coauthorship_statistics(corpus):
    sel = corpus.data["field_names"].notna()
    total_authors = corpus.data.loc[sel, "authors_wos_standard"].map(len)
    field_authors = corpus.data.loc[sel, "field_names"].map(len)
    nonfield_authors = total_authors.sub(field_authors)
    print(nonfield_authors.describe())
    print(nonfield_authors.ge(4).sum() / len(nonfield_authors))


def better_network_map(corpus, chain=("field_names",)):
    def local_network_map(block_labels, elements, characteristic, output_file):
        network_map(
            corpus,
            block_labels=block_labels,
            elements=elements,
            characteristic=characteristic,
            doc_edges="specific",
            output_file=output_file,
        )

    corpus.set_chain(*chain)
    mk, mf = corpus.make_matcher()
    dlevel = 4
    block_labels_doc = [
        label
        for label, tlb in corpus.label_to_tlblock.items()
        if tlb[0] == "doc"
        and tlb[1] == dlevel
        and corpus.dblocks[tlb[1]].eq(tlb[2]).any()
    ]
    elevel = 1
    block_labels_ext = [
        label
        for label, tlb in corpus.label_to_tlblock.items()
        if tlb[0] == "ext"
        and tlb[1] == elevel
        and corpus.eblocks[tlb[1]].eq(tlb[2]).any()
    ]
    elements_doc = [("doc", x) for k in mk for x in mf(k)]
    elements_ext = [("ext", x) for x in mk]
    characteristic_ter = {"ter": [1]}
    characteristic_ext = {"ext": [1]}
    # Alternative maps
    local_network_map(
        block_labels_doc + block_labels_ext,
        elements_doc + elements_ext,
        characteristic_ter | characteristic_ext,
        "tmp/bnm_full.pdf",
    )
    local_network_map(
        block_labels_doc,
        [],
        characteristic_ter,
        "tmp/bnm_blocks_term.pdf",
    )
    local_network_map(
        block_labels_doc + block_labels_ext,
        [],
        characteristic_ext,
        "tmp/bnm_blocks_author.pdf",
    )
    local_network_map(
        block_labels_doc + block_labels_ext,
        [],
        characteristic_ter | characteristic_ext,
        "tmp/bnm_blocks_term_author.pdf",
    )
    local_network_map(
        block_labels_doc + block_labels_ext,
        elements_doc + elements_ext,
        {},
        "tmp/bnm_elements_doc_author.pdf",
    )


def network_map_eblocks(corpus: shmi.GraphModels, elements, elevel=1):
    selection = corpus.eblocks.loc[elements, elevel].unique()
    for doc_level in corpus.eblocks_levels:
        corpus.network_map("ext", doc_level, None, 1)
        corpus.network_map("ext", doc_level, None, 1, xb_selection=selection)


def network_map_L2D(dblock_label, corpus: shmi.GraphModels, force_specific=True):
    _, dlevel, dblock = corpus.label_to_tlblock[dblock_label]
    dblocks = corpus.dblocks.loc[corpus.dblocks[dlevel].eq(dblock), 2]
    corpus.domain_network(2, 1, dblocks=dblocks, force_specific=force_specific)


def get_old_authors_domains():
    domain_labels = ["L4D2", "L4D4", "L4D12"]
    return domain_labels
