from pathlib import Path

import sashimi

from .. import data


def get_corpus():
    df = data.load_data_as_frame()
    data.text_to_tokens(df, ["title", "abstract"], "tokens")
    data.ngram_tokens(df, "tokens")
    data.authors_casefold(df)
    data.enhance_title(df)
    corpus = get_sashimi(df)
    return corpus


def get_sashimi(df):
    if not Path("auto_sashimi"):
        corpus = sashimi.GraphModels(
            col_time="year",
            col_url=["url_doi", "url_wos"],
            col_title="enhanced_title",
            col_venue="venue",
            col_id="uid",
            column="tokens",
        )
    else:
        corpus = sashimi.GraphModels(load_data=False)
    corpus.load_data(df)
    corpus.load_domain_topic_model()
    data.match_current_vocabulary(corpus)
    return corpus


def load_sampled_chained(
    corpus: sashimi.GraphModels, load=True, col_chain="field_names"
):
    corpus.set_chain(col_chain)
    corpus.set_sample(corpus.data[col_chain].notna())
    if load:
        corpus.load_domain_chained_model()
    else:
        for _ in range(100):
            corpus.load_domain_chained_model(load=False)
